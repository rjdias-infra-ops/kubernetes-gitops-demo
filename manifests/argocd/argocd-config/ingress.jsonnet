// Ingress object for the online-boutique frontend.
//
local cf = import '../../../root/cluster_functions.libsonnet';
local cv = import '../../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  local ingressName = self.metadata.name,
  apiVersion: 'networking.k8s.io/v1beta1',
  kind: 'Ingress',
  metadata: {
    annotations: {
      'nginx.ingress.kubernetes.io/backend-protocol': 'HTTPS',
    },
    name: 'argocd',
    namespace: cluster.argocdInstance.namespace,
  },
  spec: {
    rules: [
      {
        host: cf.getFqdn(ingressName, cluster),
        http: {
          paths: [
            {
              backend: {
                serviceName: 'argocd-server',
                servicePort: 443,
              },
              path: '/',
            },
          ],
        },
      },
    ],
    tls: [
      {
        hosts: [
          cf.getFqdn(ingressName, cluster),
        ],
      },
    ],
  },
}
