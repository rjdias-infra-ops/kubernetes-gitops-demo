// Ingress object for the online-boutique frontend.
//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  local ingressName = self.metadata.name,
  apiVersion: 'networking.k8s.io/v1beta1',
  kind: 'Ingress',
  metadata: {
    name: 'online-boutique',
    namespace: 'microservices-demo',
  },
  spec: {
    rules: [
      {
        host: cf.getFqdn(ingressName, cluster),
        http: {
          paths: [
            {
              backend: {
                serviceName: 'frontend',
                servicePort: 80,
              },
              path: '/',
            },
          ],
        },
      },
    ],
    tls: [
      {
        hosts: [
          cf.getFqdn(ingressName, cluster),
        ],
      },
    ],
  },
}
