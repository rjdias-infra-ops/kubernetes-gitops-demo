// Deploy a simple "Hello, World!" application.
//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

if cluster.name=='cluster001' then
{
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'Application',
  metadata: {
    annotations: {
      'argocd.argoproj.io/sync-wave': '10',
    },
    finalizers: [
      'resources-finalizer.argocd.argoproj.io',
    ],
    name: 'online-boutique',
    namespace: cluster.argocdInstance.namespace,
  },
  spec: {
    destination: {
      namespace: 'microservices-demo',
      server: 'https://kubernetes.default.svc',
    },
    project: 'default',
    source: {
      directory: {
        jsonnet: {
          extVars: cf.genExtVarsArr(),
        },
        recurse: true,
      },
      path: 'manifests/microservices-demo',
      repoURL: cluster.argocdInstance.baseRepoURL,
      targetRevision: cluster.targetRevision,
    },
    syncPolicy: {
      automated: {
        prune: true,
        selfHeal: true,
      },
    },
  },
}
