// Create Ingress objects for default cluster services.
//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'Application',
  metadata: {
    annotations: {
      'argocd.argoproj.io/sync-wave': '20',
    },
    finalizers: [
      'resources-finalizer.argocd.argoproj.io',
    ],
    name: 'ingress',
    namespace: cluster.argocdInstance.namespace,
  },
  spec: {
    destination: {
      namespace: cluster.argocdInstance.namespace,
      server: 'https://kubernetes.default.svc',
    },
    project: 'default',
    source: {
      directory: {
        jsonnet: {
          extVars: cf.genExtVarsArr(),
        },
        recurse: true,
      },
      path: 'manifests/ingress',
      repoURL: cluster.argocdInstance.baseRepoURL,
      targetRevision: cluster.targetRevision,
    },
    syncPolicy: {
      automated: {
        prune: true,
        selfHeal: true,
      },
    },
  },
}
