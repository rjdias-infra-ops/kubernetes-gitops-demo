// Create sample projects (Namespaces, and ConfigMap with info).
//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'Application',
  metadata: {
    annotations: {
      'argocd.argoproj.io/sync-wave': '5',
    },
    finalizers: [
      'resources-finalizer.argocd.argoproj.io',
    ],
    name: 'projects',
    namespace: cluster.argocdInstance.namespace,
  },
  spec: {
    destination: {
      namespace: cluster.argocdInstance.namespace,
      server: 'https://kubernetes.default.svc',
    },
    project: 'default',
    source: {
      directory: {
        jsonnet: {
          extVars: cf.genExtVarsArr(),
        },
        recurse: true,
      },
      path: 'manifests/projects',
      repoURL: cluster.argocdInstance.baseRepoURL,
      targetRevision: cluster.targetRevision,
    },
    syncPolicy: {
      automated: {
        prune: true,
        selfHeal: true,
      },
    },
  },
}
