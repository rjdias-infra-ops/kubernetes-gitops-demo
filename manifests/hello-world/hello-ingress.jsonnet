// Ingress object for the Hello App web server.
//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  local ingressName = self.metadata.name,
  apiVersion: 'networking.k8s.io/v1beta1',
  kind: 'Ingress',
  metadata: {
    name: 'hello',
  },
  spec: {
    rules: [
      {
        host: cf.getFqdn(ingressName, cluster),
        http: {
          paths: [
            {
              backend: {
                serviceName: 'hello-svc',
                servicePort: 80,
              },
              path: '/',
            },
            {
              backend: {
                serviceName: 'hello-svc-v1',
                servicePort: 80,
              },
              path: '/v1',
            },
            {
              backend: {
                serviceName: 'hello-svc-v2',
                servicePort: 80,
              },
              path: '/v2',
            },
          ],
        },
      },
    ],
    tls: [
      {
        hosts: [
          cf.getFqdn(ingressName, cluster),
        ],
      },
    ],
  },
}
