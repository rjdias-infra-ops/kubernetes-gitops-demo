//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

local namespace = 'backend';

cf.createProject(namespace, cluster)
