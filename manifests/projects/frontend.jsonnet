//
local cf = import '../../root/cluster_functions.libsonnet';
local cv = import '../../root/cluster_vars.libsonnet';
local cluster = cv.cluster;

local namespace = 'frontend';
local additionalObjects = [
  {
    apiVersion: 'v1',
    kind: 'ResourceQuota',
    metadata: {
      name: 'default-compute-quota',
      namespace: namespace,
    },
    spec: {
      hard: {
        'limits.cpu': '2',
        'limits.memory': '2Gi',
        'requests.cpu': '1',
        'requests.memory': '1Gi',
      },
    },
  },
];

cf.createProject(namespace, cluster) + additionalObjects
