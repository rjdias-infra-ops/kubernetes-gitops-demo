// App of Apps pattern example.
//
local cf = import 'cluster_functions.libsonnet';
local cv = import 'cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'Application',
  metadata: {
    annotations: {
      'argocd.argoproj.io/sync-wave': '0',
    },
    finalizers: [
      'resources-finalizer.argocd.argoproj.io',
    ],
    name: 'app-of-apps-utilities',
    namespace: cluster.argocdInstance.namespace,
  },
  spec: {
    destination: {
      namespace: cluster.argocdInstance.namespace,
      server: 'https://kubernetes.default.svc',
    },
    project: 'default',
    source: {
      directory: {
        jsonnet: {
          extVars: cf.genExtVarsArr(),
        },
        recurse: true,
      },
      path: 'manifests/app-of-apps-utilities',
      repoURL: cluster.argocdInstance.baseRepoURL,
      targetRevision: cluster.targetRevision,
    },
    syncPolicy: {
      automated: {
        prune: true,
        selfHeal: true,
      },
    },
  },
}
