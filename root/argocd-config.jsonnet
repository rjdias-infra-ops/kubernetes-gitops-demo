// Configure ArgoCD with ArgoCD.
//
local cf = import 'cluster_functions.libsonnet';
local cv = import 'cluster_vars.libsonnet';
local cluster = cv.cluster;

{
  apiVersion: 'argoproj.io/v1alpha1',
  kind: 'Application',
  metadata: {
    annotations: {
      'argocd.argoproj.io/sync-wave': '0',
    },
    finalizers: [
      'resources-finalizer.argocd.argoproj.io',
    ],
    name: 'argocd-config',
    namespace: cluster.argocdInstance.namespace,
  },
  spec: {
    destination: {
      namespace: cluster.argocdInstance.namespace,
      server: 'https://kubernetes.default.svc',
    },
    project: 'default',
    source: {
      directory: {
        jsonnet: {
          extVars: cf.genExtVarsArr(),
        },
        recurse: true,
      },
      path: 'manifests/argocd/argocd-config',
      repoURL: cluster.argocdInstance.baseRepoURL,
      targetRevision: cluster.targetRevision,
    },
    syncPolicy: {
      automated: {
        prune: false,
        selfHeal: true,
      },
    },
  },
}
