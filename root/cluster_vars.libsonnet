// Cluster-wide available variables.
//
local ce = import 'cluster_extvar.libsonnet';
local cf = import 'cluster_functions.libsonnet';

{
  defaultCluster: {
    baseDomain: 'default.test',
    name: 'default',
    region: 'default',
    targetRevision: ce.targetRevision,
    argocdInstance: {
      baseRepoURL: 'https://',
      namespace: 'argocd',
    },
    monitoringInstance: {
      name: 'prometheus-operator',
      namespace: 'kube-monitoring',
    },
  },
  clusters: [
    self.defaultCluster {
      baseDomain: 'clusters.rdias.xyz',
      name: 'cluster001',
      region: 'eu-central-1',
      targetRevision: 'dev',
      argocdInstance+: {
        baseRepoURL: 'https://gitlab.com/rjdias-infra-ops/kubernetes-gitops-demo.git',
      },
    },
    self.defaultCluster {
      baseDomain: 'clusters.rdias.xyz',
      name: 'cluster002',
      region: 'eu-central-1',
      targetRevision: 'master',
      argocdInstance+: {
        baseRepoURL: 'https://gitlab.com/rjdias-infra-ops/kubernetes-gitops-demo.git',
      },
    },
    self.defaultCluster {
      baseDomain: 'clusters.rdias.xyz',
      name: 'localhost',
      region: 'localhost',
      argocdInstance+: {
        baseRepoURL: 'https://gitlab.com/rjdias-infra-ops/kubernetes-gitops-demo.git',
      },
    },
  ],
  cluster: cf.getCluster(self.clusters, ce.loadCluster),
}
