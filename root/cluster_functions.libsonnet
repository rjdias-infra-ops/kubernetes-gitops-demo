// Cluster-wide available functions.
//
local ce = import 'cluster_extvar.libsonnet';
local cv = import 'cluster_vars.libsonnet';

{
  // Get a cluster object given it's name and an array of clusters.
  // Returns an error if cluster not found.
  //
  getCluster(clusters_arr, clusterName)::
    local filtered = std.filter(function(cluster) cluster.name == clusterName, clusters_arr);
    if filtered != [] then filtered[0] else error ('Cluster "' + clusterName + '" not found!'),

  // Get the FQDN for a Service based on cluster information.
  //
  getFqdn(hostname, cluster)::
    hostname + '.' + cluster.name + '.' + cluster.baseDomain,

  // One standard way of creating a project.
  // Namespace, ConfigMap with cluster data,
  // ResourceQuotas, Limits, etc.
  //
  createProject(namespace, cluster):: [
    {
      apiVersion: 'v1',
      kind: 'Namespace',
      metadata: {
        name: namespace,
      },
    },
    {
      apiVersion: 'v1',
      kind: 'ConfigMap',
      metadata: {
        name: 'cluster-info',
        namespace: namespace,
      },
      data: {
        baseDomain: cluster.baseDomain,
        name: cluster.name,
        region: cluster.region,
        targetRevision: cluster.targetRevision,
        baseRepoURL: cluster.argocdInstance.baseRepoURL,
      },
    },
    {
      apiVersion: 'v1',
      kind: 'ResourceQuota',
      metadata: {
        name: 'default-object-quota',
        namespace: namespace,
      },
      spec: {
        hard: {
          configmaps: '20',
          persistentvolumeclaims: '3',
          secrets: '20',
          services: '10',
          'services.loadbalancers': '1',
          'services.nodeports': '0',
        },
      },
    },
  ],

  // Generate a list of extVars given the Cluster's extVars object (ce).
  // This serves to pass extVars from Application to Applicationon on
  // an App of Apps without
  // manually adding all of the keys.
  //
  genExtVarsArr():: [
    {
      name: var,
      value: std.extVar(var),
    }
    for var in std.objectFields(ce)
  ],
}
