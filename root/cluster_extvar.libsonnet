// Cluster-wide available external variables.
//
{
  loadCluster: std.extVar('loadCluster'),
  targetRevision: std.extVar('targetRevision'),
}
