#!/usr/bin/env bash
set -eux;

# Environment variables.
LOADCLUSTER="localhost"
TARGETREVISION="master"

# Deploy ArgoCD + git repos + keys + configs.
kubectl apply -k manifests/argocd/
kubectl -n argocd rollout status deployment/argocd-server -w

jsonnet \
--ext-str loadCluster=${LOADCLUSTER} \
--ext-str targetRevision=${TARGETREVISION} \
root/argocd-config.jsonnet | kubectl apply -f -

# Create the root of all evil.
jsonnet \
--ext-str loadCluster=${LOADCLUSTER} \
--ext-str targetRevision=${TARGETREVISION} \
root/app-of-apps-utilities.jsonnet | kubectl apply -f -

