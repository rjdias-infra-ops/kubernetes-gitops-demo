# Demo repository for GitOps on Kubernetes using ArgoCD
This repository will help you manage/operate your Kubernetes clusters from Git.  
It will showcase a multitude of ArgoCD functionality in combination with templating frameworks.  

## Features:
* Cluster cloning or recreation for disaster recovery (excluding persistent storage).
* Coordinate global roll-outs.
* Customizability. Build the strategy your way!
* Define environments with/as code.
* Kubernetes Secrets on Git (with Bitnami Sealed Secrets).

With this code you will deploy ArgoCD and a "root" base manifest which will load everything else from git and your cluster will become operated from git (GitOps).

## How to use:
* Deploy your Kubernetes cluster.
* Install `jsonnet` and `kubectl`.
  - Optionally install `argocd`, `helm`, `kubeseal` and `kustomize` tools.
* Set up cluster access credentials for `kubectl`.
* Deploy with:  
  ```bash
  bash bootstrap.sh
  ```
* Get the ArgoCD web interface address with:
  ```bash
  kubectl get svc argocd-server -n argocd
  ```
* Paste that address on your browser and login with:
  - Username: admin
  - Password: 00000000

## Tools:
### ArgoCD
> Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.

ArgoCD's vision is that:
> Application definitions, configurations, and environments should be declarative and version controlled.  
> Application deployment and lifecycle management should be automated, auditable, and easy to understand.

Example of an ArgoCD `Application` CRD object:
```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: hello
  namespace: argocd
spec:
  source:
    repoURL: "https://gitlab.com/rjdias-infra-ops/kubernetes-gitops-demo.git"
    path: manifests/hello-world
    targetRevision: dev
  destination:
    namespace: hello
    server: "https://kubernetes.default.svc"
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

* `.spec.source` is the source of the desired state. That includes the git repo, folder and branch where you want to get the manifests. Also, some environment configuration (eg: helm values.yaml).
* `.spec.destination` defines the target cluster and namespace to deploy the manifests.

After you are familiar with the ArgoCD CRDs you can create a root CRD that in turn will sync more ArgoCD CRDs in a recursive fashion.

> You can create an app that creates other apps, which in turn can create other apps.  
> This allows you to declaratively manage a group of apps that can be deployed and configured in concert.

What you will end up with is a chain of manifests that will form your entire cluster as a whole.

### Bitnami Sealed Secrets
TODO.

> Problem: "I can manage all my K8s config in git, except Secrets."  
>
> Solution: Encrypt your Secret into a SealedSecret, which is safe to store - even to a public repository. The SealedSecret can be decrypted only by the controller running in the target cluster and nobody else (not even the original author) is able to obtain the original Secret from the SealedSecret.

(quote from sealed secrets project docs)

## Manifest templating:
### Helm
* Known as the Kubernetes package manager.
* Very good to be in sync with vendor provided applications such as big software stacks.

### Jsonnet
* A powerful DSL for elegant description of JSON data.
* An extension of JSON.
* Functional programming paradigm language.

### Kustomize
* Lets you customize raw, template-free YAML, leaving the original YAML untouched and usable as is.
* Useful to fuse your YAML manifests with upstream manifests.
* Comes embedded with `kubectl`.

## Notes:
* A cluster with an `Ingress controller` and `Service .spec.type: LoadBalancer` capabilities are required for the full demo as d
* Alternative ways of exposing the ArgoCD Server service are on their project documentation.
* Hard-coded password for demo purposes, don't use it in production.
* The quotes are from the respective projects that are being shown here.

## Resources:
ArgoCD documentation:  
[Getting Started/Access The Argo CD API Server][argocd-getting-started-access-the-argo-cd-api-server]  
[Cluster Bootstrapping/App Of Apps Pattern][argocd-bootstrapping-app-of-apps]  
[Declarative Setup/App of Apps][argocd-declarative-app-of-apps]  

Bitnami Sealed Secrets documentation:  
[Sealed Secrets/Overview][sealed-secrets-overview]  
[Sealed Secrets/Secret Rotation][sealed-secrets-rotation]  

Helm documentation:  
[Intro/Installing Helm][helm-intro-install]  
[Intro/Quickstart Guide][helm-intro-quickstart]  
[Intro/Quickstart Guide/Initialize a Helm Chart Repository][helm-intro-quickstart-initialize-chart-repository]  
[Intro/Quickstart Guide/Install an Example Chart][helm-intro-quickstart-install]  

Jsonnet documentation:  
[Articles/Kubernetes][jsonnet-articles-kubernetes]  
[Learning/Getting Started][jsonnet-learning-getting-started]  
[Learning/Tutorial][jsonnet-learning-tutorial]  
[Reference/Jsonnet Specification][jsonnet-ref-spec]  
[Reference/Standard Library][jsonnet-ref-stdlib]  

Kustomize documentation:  
[Examples][kustomize-examples]  


[argocd-bootstrapping-app-of-apps]: https://argoproj.github.io/argo-cd/operator-manual/cluster-bootstrapping/#app-of-apps-pattern
[argocd-declarative-app-of-apps]: https://argoproj.github.io/argo-cd/operator-manual/declarative-setup/#app-of-apps
[argocd-getting-started-access-the-argo-cd-api-server]: https://argoproj.github.io/argo-cd/getting_started/#3-access-the-argo-cd-api-server
[helm-intro-install]: https://helm.sh/docs/intro/install/
[helm-intro-quickstart-initialize-chart-repository]: https://helm.sh/docs/intro/quickstart/#initialize-a-helm-chart-repository
[helm-intro-quickstart-install]: https://helm.sh/docs/intro/quickstart/#install-an-example-chart
[helm-intro-quickstart]: https://helm.sh/docs/intro/quickstart/
[jsonnet-articles-kubernetes]: https://jsonnet.org/articles/kubernetes.html
[jsonnet-learning-getting-started]: https://jsonnet.org/learning/getting_started.html
[jsonnet-learning-tutorial]: https://jsonnet.org/learning/tutorial.html
[jsonnet-ref-spec]: https://jsonnet.org/ref/spec.html
[jsonnet-ref-stdlib]: https://jsonnet.org/ref/stdlib.html
[kustomize-examples]: https://github.com/kubernetes-sigs/kustomize/tree/master/examples
[sealed-secrets-rotation]: https://github.com/bitnami-labs/sealed-secrets#secret-rotation
[sealed-secrets-overview]: https://github.com/bitnami-labs/sealed-secrets#overview

